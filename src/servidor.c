#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>            
#include <stddef.h>            
#include <string.h>             
#include <unistd.h>            
#include <signal.h>             
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	

#define QUEUESIZE 50

int main( int argc, char *argv[]){
	//formato para el uso del ejecutable
	if(argc == 1){
		printf("Uso: ./servidor <direccion> <numero de puerto>\n");
		exit(-1);
	}
	if(argc != 3){
		printf("Ingrese la cantidad de argumentos validos\n");
	}
	//se reciben los argumentos del comando
	int port = atoi(argv[2]);
	//se crea socket para el servidor, se setea la memoria en 0 y se arma la estructura
	struct sockaddr_in server;
	memset(&server, 0, sizeof(server));	
	server.sin_family = AF_INET;		
	server.sin_port = htons(port);	
	server.sin_addr.s_addr = inet_addr(argv[1]) ;
	//Se crea el socket y se validan bin y listen para continuar con la conexion
	int fd;	
	if((fd = socket(server.sin_family, SOCK_STREAM, 0)) < 0){
		perror("Error al crear socket servidor\n");
		return -1;
	}
	if(bind(fd, (struct sockaddr *)&server, sizeof(server)) < 0){
		perror("Error en bind servidor\n");
		return -1;	
	}
	if(listen(fd, QUEUESIZE) < 0){
		perror("Error en listen servidor\n");
		return -1;		
	}
	
	while(1){
		//se acepta la conexion y se crea un nuevo socket reemplazando al anterior
		int sockfd_conectado = accept(fd, NULL, 0);
		printf("Se conecto alguien\n");
		//este buffer nos sirve para almacenar la direccion de origen que manda el cliente
		char buffer1[1000] = {0};
		int leidos = read(sockfd_conectado, buffer1, 1000);
		if(leidos < 0){
			perror("Error al leer sockedfd_conectado\n");
            return -1;
		}
		//se abre el archivo de origen
        int fd_fuente = open(buffer1, O_RDONLY);
        if(fd_fuente<0){
            perror("Error al abrir el archivo fd_fuente\n");
            return -1;
        }
		//este buffer nos sirve para almacenar los bytes del archivo de origen
		unsigned long int *buffer2 = calloc(10000, sizeof(unsigned long));
		int env = 0;
		//se leen los bytes del archivo de origen y se los envia al socket del cliente con write
        while((env = read(fd_fuente, buffer2, 1000)) != 0){
            if(env<0){
                perror("Error al leer el archivo fd_duente\n");
                return 0;
            }
            write(sockfd_conectado, buffer2, env);
        }
		//se cierran los descriptores creados
        if (close(fd_fuente)<0){
            printf("Error al cerrar fd_fuente\n");
            return -1;
        }    
		if (close(sockfd_conectado)<0){
            printf("Error al cerrar el socket_conectado\n");
            return -1;
        }
		printf("Archivo ya duplicado\n");
	}
		
}
