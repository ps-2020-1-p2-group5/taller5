#include <sys/types.h>         
#include <sys/stat.h>
#include <stdio.h>            
#include <stdlib.h>            
#include <stddef.h>            
#include <string.h>            
#include <unistd.h>             
#include <signal.h>            
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif
#define QUEUESIZE 10

int main( int argc, char *argv[]){
	//formato para el uso del ejecutable
	if(argc == 1){
		printf("Uso: ./cliente <direccion> <numero de puerto> <ruta_local> <ruta_remota>\n");
		exit(-1);
	}
	if(argc != 5){
		printf("Ingrese la cantidad de argumentos validos\n");
	}
	//se reciben los argumentos del comando
	int port = atoi(argv[2]);
    char *origin = argv[3];
    char *dest = argv[4];
	//se crea socket para el cliente, se setea la memoria en 0 y se arma la estructura
	struct sockaddr_in client;
	memset(&client, 0, sizeof(client));
	client.sin_family = AF_INET;
	client.sin_port = htons(port);
	client.sin_addr.s_addr = inet_addr(argv[1]) ;
	int fd;
	if((fd = socket(client.sin_family, SOCK_STREAM, 0)) < 0){
		perror("Error al crear socket\n");
		return -1;
	}
	//se crea y valida la conexion
	int response = connect(fd, (struct sockaddr *)&client, sizeof(client));
	if(response < 0){
		close(fd);
		perror("Error en connect cliente\n");
		exit(-1);
	}
	//se crea el archivo donde se copiara todo lo que envie el servidor si no hay problemas
    umask(0);
    int fd_dest = open(dest, O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if(fd_dest<0){
        perror("Error al abrir el archivo fd_destino\n");
        exit(-1);
    }
	//se envia la ruta de origen al servidor y se espera una respuesta
	write(fd, origin, 1000);
    char buffer[1000] = {0};
    int res = 0;
	//se lee y escribe la respuesta en el archivo de copia o destino
    while((res = read(fd, buffer, 1000)) > 0){
        if(res<0){
            perror("Error al leer el archivo origen\n");
            exit(-1);
        }
        write(fd_dest, buffer, res);
    }
	//se cierran los descriptores y sockets
    if (close(fd_dest)<0){
        printf("Error al cerrar fd_destino\n");
        exit(-1);
    }
	if(close(fd) < 0){
        printf("Error al cerrar el socket cliente\n");
        exit(-1);
    }
    return 0;

}
